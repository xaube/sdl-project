# sdl-project

Projeto para praticar SDL

## Requisitos

É necessário um conjunto de bibliotecas, que são:

* libsdl2-dev
* libsdl2-image-dev
* libsdl2-mixer-dev
* libsdl2-ttf-dev

Caso seja necessário a adição de mais libs, serão adicionadas neste readme.
