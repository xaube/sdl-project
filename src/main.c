#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>

// Global vars
int SCRN_WIDTH = 1280;
int SCRN_HEIGHT = 720;

SDL_Window *gWindow = NULL;
SDL_Renderer *gRenderer = NULL;

int init();
int load_media();
void close();
// signal handlers
int catch_signal(int sig, void (*handler)(int));
void goodbye(int);


int main(int argc, char *argv[]){	
	if(!init()){
		printf("Failed to initialize game.\n");
	}else{
		int quit = 0;
		SDL_Event e;
		while(!quit){
			while(SDL_PollEvent(&e) != 0){
				if(e.type == SDL_QUIT) quit = 1;
			}
		}
	}
	close();
	return 0;
}

int init(){
	int success = 1;
	// Initialize interruption handler
	if(catch_signal(SIGINT, goodbye) == -1){
		puts("Can't map signal handler.\n");
	}
	// Initialize SDL
	if(SDL_Init(SDL_INIT_VIDEO) != 0){
		success = 1;
		printf("Failed to initialize SDL | SDL_Error: %s\n", SDL_GetError());
	}else{
		// Create window
		gWindow = SDL_CreateWindow("Window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCRN_WIDTH, SCRN_HEIGHT, SDL_WINDOW_SHOWN);
		if(gWindow == NULL){
			success = 0;
			printf("Failed to create window | SDL_Error: %s\n", SDL_GetError());
		}else{
			// Create renderer
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if(gRenderer == NULL){
				success = 0;
				printf("Failed to create renderer | SDL_Error: %s\n", SDL_GetError());
			}else{
				// Initialize IMG
				int imgFlags = IMG_INIT_PNG;
				if(!(IMG_Init(imgFlags)&imgFlags)){
					success = 0;
					printf("Failed to initialize SDL_image | IMG_error: %s\n", IMG_GetError());
				}
			}
		}
	}
	return success;
}

int catch_signal(int sig, void (*handler)(int)){
        struct sigaction action;
        action.sa_handler = handler;
        sigemptyset(&action.sa_mask);
        action.sa_flags = 0;
        return sigaction(sig, &action, NULL);
}

void close(){
	puts("Goodbye");
	SDL_DestroyWindow(gWindow);
	SDL_DestroyRenderer(gRenderer);
	IMG_Quit();
	SDL_Quit();
}

void goodbye(int sig){
	puts("\nGoodbye...");
	exit(1);
}
